﻿using System;

namespace Exercise_7
{
    class Program
    {
        static void Main(string[] args)
        {
            var userIn = 0;
            var index = 20;

            Console.WriteLine("Enter an integer that is less than 20");
            userIn = int.Parse(Console.ReadLine());

            do
            {
                Console.WriteLine($"This is line {userIn + 1}");
                userIn++;

            } while (userIn < index);

            Console.ReadLine();
        }
    }
}
