﻿using System;

namespace Exercise_6
{
    class Program
    {
        static void Main(string[] args)
        {
            var count = 6;
            var i = 0;

            do
            {
                Console.WriteLine($"This is line {i + 1}");
                i++;
            } while (i < 6);

            Console.ReadLine();
        }
    }
}
