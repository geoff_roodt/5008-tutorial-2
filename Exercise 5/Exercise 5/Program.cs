﻿using System;

namespace Exercise_5
{
    class Program
    {
        static void Main(string[] args)
        {
            var welcomeQuestion = "Hello User";
            var score = 0;
            var userAnswer = string.Empty;

            Console.WriteLine("Enter True (t) or False (f) to the following questions");
            Console.WriteLine("Is the Sky Blue?");
            userAnswer = Console.ReadLine().ToLower();

            if (userAnswer == "true" || userAnswer == "false")
            {
                var myAnswer = bool.Parse(userAnswer);
                if (!myAnswer)
                {
                    Console.WriteLine($"Your Score was: {score}");
                    Console.ReadLine();
                }

                score++;
                Console.WriteLine("Are there 26 Letters in the Alphabet?");
                userAnswer = Console.ReadLine().ToLower();

                if (userAnswer == "true" || userAnswer == "false")
                {
                    myAnswer = bool.Parse(userAnswer);
                    if (!myAnswer)
                    {
                        Console.WriteLine($"Your Score was: {score}");
                        Console.ReadLine();
                    }

                    score++;
                    Console.WriteLine("Is Geoff a Man?");
                    userAnswer = Console.ReadLine().ToLower();
                    if (userAnswer == "true" || userAnswer == "false")
                    {
                        myAnswer = bool.Parse(userAnswer);
                        if (myAnswer)
                        {
                            score++;
                        }
                    }
                    Console.WriteLine("You have finished the game!!");
                    Console.WriteLine($"Your Score was: {score}");
                    Console.ReadLine();
                }
            }
        }
    }
}
