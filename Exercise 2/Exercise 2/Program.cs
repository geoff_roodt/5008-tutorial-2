﻿using System;

namespace Exercise_2
{
    class Program
    {
        const string Red = "red";
        const string Blue = "blue";
        static void Main(string[] args)
        {
            // Define 2 colours
            
            Console.WriteLine("Please type in a colour name");
            var userColour = Console.ReadLine();

            switch (userColour.ToLower()) // Alert the user to the fact that they have chosen one of the secret colours
            {
                case Red:
                    Console.WriteLine("You chose red - Apples can be red");
                    break;
                case Blue:
                    Console.WriteLine("You chose blue - The sky is blue");
                    break;
                default:
                    Console.WriteLine("You chose a wrong colour");
                    break;
            }

            Console.ReadLine();
            
        }
    }
}
