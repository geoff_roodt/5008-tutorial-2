﻿using System;

namespace Exercise_3
{
    class Program
    {
        const double Kilometer = 0.621371; // Initialise the constants for unit conversion
        const double Mile = 1.609344;

        static void Main(string[] args)
        {
            double convertedUnit = 0; // The final measurement post-conversion
            double userInput = 0; // The value the user wants to convert
            var userChoice = string.Empty; // The choices of the user

            Console.WriteLine("Enter 'Kilometers' to convert to Kilometers");
            Console.WriteLine("Or enter 'Miles' to convert to Miles");

            userChoice = Console.ReadLine().ToLower();
            switch (userChoice)
            {
                case "kilometers":
                    Console.WriteLine("Enter the unit of Miles");
                    userChoice = Console.ReadLine();

                    if (double.TryParse(userChoice, out userInput))
                    {
                        var MyConvertedUnit = userInput / Kilometer;
                        Console.WriteLine($"{userInput} Miles = {MyConvertedUnit} Kilometers");
                    }
                    else
                    {
                        Console.WriteLine("Unable to convert to Kilometers!");
                    }
                    break;

                case "miles":
                    Console.WriteLine("Enter the unit of Kilometers");
                    userChoice = Console.ReadLine();

                    if (double.TryParse(userChoice, out userInput))
                    {
                        var MyConvertedUnit = userInput / Mile;
                        Console.WriteLine($"{userInput} Kilometers = {MyConvertedUnit} Miles");
                    }
                    else
                    {
                        Console.WriteLine("Unable to convert to Miles!");
                    }
                    break;

                default:
                    Console.WriteLine("No proper calculation was selected");
                    break;


            }


        }
    }
}
