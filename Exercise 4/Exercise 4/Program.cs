﻿using System;

namespace Exercise_4
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 7;
            for(int i = 0; i < counter; i++)
            {
                Console.WriteLine($"Position {i}");
            }
        }
    }
}
