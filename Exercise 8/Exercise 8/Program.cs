﻿using System;

namespace Exercise_8
{
    class Program
    {
        static void Main(string[] args)
        {
            var respond = "Please respond to this (true or false)";
            Console.WriteLine("This is a statement");
            Console.WriteLine(respond);

            var userIn = bool.Parse(Console.ReadLine());
            var otherIn = string.Empty;

            if (userIn)
                otherIn = "true";
            else
                otherIn = "false";

            Console.WriteLine($"The answer is: {otherIn}");
            Console.ReadLine();

        }
    }
}
