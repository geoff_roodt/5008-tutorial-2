﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var number1 = 10;
            var number2 = 20;

            if (number1 > number2)
            {
                Console.WriteLine("Number 1 is greater than Number 2");
            }
            else
            {
                Console.WriteLine("Number 1 is smaller than or equal to Number 2");
            }
        }
    }
}
