﻿using System;

namespace Exercise_9
{
    class Program
    {
        static void Main(string[] args)
        {
            int numLines = 0; // Will be the number of lines to display
            string userInput = string.Empty;
            var userCancel = false;

            Console.WriteLine("To end, type 'end' at the start of each round");
            do
            {
                Console.WriteLine("How many lines to display?");

                userInput = Console.ReadLine();
                if (userInput.ToString().ToLower() == "end")
                {
                    userCancel = true;
                }
                else if (int.TryParse(userInput, out numLines))
                {
                    if (numLines == 0)
                        Console.WriteLine("Line number: 0");
                    else
                    {
                        for (int i = 0; i < numLines; i++)
                            Console.WriteLine($"Line number: {i + 1}");
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Input, Please Input a Valid Entry");
                }

            } while (!userCancel);

            Console.WriteLine("User has exited");
            Console.ReadLine();
        }
    }
}
